package com.example.technical_task.view;

import com.example.technical_task.model.Matching;
import com.example.technical_task.model.MatchingList;
import java.util.Collections;
import java.util.List;

public class View {
    public static String styleEmployeeList(List<MatchingList> matching) {
        String message = "";
        Collections.sort(matching);
        for (int i = matching.size() - 1; i > 0; i--) {
            MatchingList matchingList = matching.get(i);
            for (Matching m : matchingList.getMatching()) {
                String employee1 = m.getEmployee1() != null ? m.getEmployee1().getName() : null;
                String employee2 = m.getEmployee2() != null ? m.getEmployee2().getName() : null;
                message = message + " <b>" + employee1 + "</b> with <b>" + employee2 + "</b> = "
                        + m.getMatching() + "% " + "<br>";
            }
            message = message + "Average score" + " = " + matchingList.getAverage() + "% <br> <br>";
        }
        return message;
    }

    public static String styleErrorCases(String message) {
        return "<h1 style=" + "color:red" + ">" + message + "</h1>";
    }
}
