package com.example.technical_task.model;

import java.util.List;

public class MatchingList implements Comparable {
    private List<Matching> matching;
    private int average;

    public List<Matching> getMatching() {
        return matching;
    }

    public void setMatching(List<Matching> matching) {
        this.matching = matching;
    }

    public int getAverage() {
        return average;
    }

    public void setAverage(int average) {
        this.average = average;
    }


    @Override
    public int compareTo(Object o) {
        int average = ((MatchingList) o).getAverage();
        return this.average - average;
    }
}
