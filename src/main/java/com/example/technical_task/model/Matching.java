package com.example.technical_task.model;

public class Matching {
    private Employee employee1;
    private Employee employee2;
    private int matching;

    public Matching() {
    }

    public Matching(Employee employee1, Employee employee2, int matching) {
        this.employee1 = employee1;
        this.employee2 = employee2;
        this.matching = matching;
    }

    public Employee getEmployee1() {
        return employee1;
    }

    public void setEmployee1(Employee employee1) {
        this.employee1 = employee1;
    }

    public Employee getEmployee2() {
        return employee2;
    }

    public void setEmployee2(Employee employee2) {
        this.employee2 = employee2;
    }

    public int getMatching() {
        return matching;
    }

    public void setMatching(int matching) {
        this.matching = matching;
    }

}
