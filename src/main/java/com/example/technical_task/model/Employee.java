package com.example.technical_task.model;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvBindByPosition;

public class Employee {
    //  @CsvBindByName(column = "Name")

    @CsvBindByPosition(position = 0)
    private String name;
    @CsvBindByPosition(position = 1)
    private String email;
    @CsvBindByPosition(position = 2)
    private String division;
    @CsvBindByPosition(position = 3)
    private int age;
    @CsvBindByPosition(position = 4)
    private int timezone;

    public Employee() {
    }

    public Employee(String name, String email, String division, int age, int timezone) {
        this.name = name;
        this.email = email;
        this.division = division;
        this.age = age;
        this.timezone = timezone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDivision() {
        return division;
    }

    public void setDivision(String division) {
        this.division = division;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTimezone() {
        return timezone;
    }

    public void setTimezone(int timezone) {
        this.timezone = timezone;
    }
}
