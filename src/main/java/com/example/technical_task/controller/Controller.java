package com.example.technical_task.controller;

import com.example.technical_task.service.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
public class Controller {
    @Autowired
    private final Service service;

    public Controller(Service service) {
        this.service = service;
    }


    @PostMapping("/upload-file")
    public String uploadCsvFile(@RequestParam("file") MultipartFile multipartFile, ModelMap model) throws IOException {
        return service.uploadFile(multipartFile);
    }
}
