package com.example.technical_task.service;

import com.example.technical_task.model.Employee;
import com.example.technical_task.model.Matching;
import com.example.technical_task.model.MatchingList;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import org.springframework.ui.Model;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.*;

import static com.example.technical_task.service.compute.Compute.computeAverage;
import static com.example.technical_task.service.compute.Compute.computeMatching;
import static com.example.technical_task.view.View.styleEmployeeList;
import static com.example.technical_task.view.View.styleErrorCases;


@org.springframework.stereotype.Service
public class Service {

    public String uploadFile(MultipartFile file) throws IOException {
        Map map = new LinkedHashMap();
        try (Reader reader = new BufferedReader(new InputStreamReader(file.getInputStream()))) {
            CsvToBean<Employee> csvToBean = new CsvToBeanBuilder<Employee>(reader)
                    .withType(Employee.class)
                    .withSkipLines(1)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build();
            List<Employee> employees = csvToBean.parse();
            String error = checkErrorCases(employees);
            if (error != null) {
                return error;
            }
            return styleEmployeeList(pairing(employees));
        } catch (Exception ex) {
        }
        return null;
    }

    private static List<MatchingList> pairing(List<Employee> list) {
        if (list.size() == 2) {
            List<MatchingList> result = new LinkedList<>();
            MatchingList resMatching = new MatchingList();
            Employee employee1 = list.get(0);
            Employee employee2 = list.get(1);
            int computeMatching = computeMatching(employee1, employee2);
            Matching matching = new Matching(employee1, employee2, computeMatching);
            List<Matching> matchingList = new LinkedList<>();
            matchingList.add(matching);
            resMatching.setMatching(matchingList);
            resMatching.setAverage(computeAverage(matchingList));
            result.add(resMatching);
            return result;
        }
        int n = list.size();
        if (n % 2 == 0) {
            List<Employee> newEmployeeList = new LinkedList(list);
            Employee employee2 = list.get(list.size() - 1);
            newEmployeeList.remove(list.size() - 1);
            List<MatchingList> matchingLists = pairing(newEmployeeList);
            for (MatchingList m : matchingLists) {
                for (Matching mm : m.getMatching()) {
                    if (mm.getEmployee2() == null) {
                        mm.setEmployee2(employee2);
                        mm.setMatching(computeMatching(mm.getEmployee1(), mm.getEmployee2()));
                    }
                }
                m.setAverage(computeAverage(m.getMatching()));
            }
            return matchingLists;
        } else {
            List<MatchingList> matchingLists = new LinkedList<>();
            for (int i = 0; i < list.size(); i++) {
                List<Employee> removeList = new LinkedList<>(list);
                Employee removeEmployee = removeList.get(i);
                removeList.remove(i);
                List<MatchingList> result = pairing(removeList);
                Matching matching1 = new Matching();
                matching1.setEmployee1(removeEmployee);
                matching1.setEmployee2(null);
                matching1.setMatching(computeMatching(removeEmployee, null));
                for (MatchingList resultMatching : result) {
                    resultMatching.getMatching().add(matching1);
                    resultMatching.setAverage(computeAverage(resultMatching.getMatching()));
                }
                matchingLists.addAll(result);
            }
            return matchingLists;
        }
    }

    private String checkErrorCases(List<Employee> list) {
        String message = "";
        if (list.size() < 3) {
            return styleErrorCases("Error Employee count");
        }
        return null;
    }


}

