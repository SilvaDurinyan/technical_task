package com.example.technical_task.service.compute;

import com.example.technical_task.model.Employee;
import com.example.technical_task.model.Matching;

import java.util.List;

public class Compute {
    private static final int ageScore = 30;
    private static final int divisionScore = 30;
    private static final int timezoneScore = 40;


    public static int computeMatching(Employee employee1, Employee employee2) {
        int value = 0;
        if (employee1 == null || employee2 == null) {
            return value;
        }
        if (employee1.getDivision().equalsIgnoreCase(employee2.getDivision())) value = value + divisionScore;
        if (employee1.getTimezone() == employee2.getTimezone()) value = value + timezoneScore;
        if (Math.abs(employee1.getAge() - employee2.getAge()) <= 5) value = value + ageScore;
        return value;
    }


    public static int computeAverage(List<Matching> matchingList) {
        int value = 0;
        for (Matching matching : matchingList) {
            value = value + matching.getMatching();
        }
        return value / 2;
    }
}
